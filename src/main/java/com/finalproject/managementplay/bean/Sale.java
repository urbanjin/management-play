package com.finalproject.managementplay.bean;

public class Sale {
    private int id;
    private String name;
    private int stock;
    private int plan;
    private int sold;

    public Sale() {
    }

    public Sale(String name, int stock, int plan, int sold) {
        this.name = name;
        this.stock = stock;
        this.plan = plan;
        this.sold = sold;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPlan() {
        return plan;
    }

    public void setPlan(int plan) {
        this.plan = plan;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stock=" + stock +
                ", plan=" + plan +
                ", sold=" + sold +
                '}';
    }
}
