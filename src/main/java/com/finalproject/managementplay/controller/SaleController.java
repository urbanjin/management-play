package com.finalproject.managementplay.controller;

import com.alibaba.fastjson.JSON;
import com.finalproject.managementplay.bean.QueryInfo;
import com.finalproject.managementplay.bean.Sale;
import com.finalproject.managementplay.dao.SaleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class SaleController {
    @Autowired
    private SaleDao sdao;

    @RequestMapping("/allsale")
    public String getSaleList(QueryInfo queryInfo){
        //获取最大列表数字和当前编号
        int numbers = sdao.getSaleCounts("%" + queryInfo.getQuery() + "%");
        int pageStart = (queryInfo.getPageNum() - 1) * queryInfo.getPageSize();

        List<Sale> sales = sdao.getAllSale("%" + queryInfo.getQuery() + "%", pageStart, queryInfo.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", sales);
        String res_string = JSON.toJSONString(res);
        return res_string;
    }
}
