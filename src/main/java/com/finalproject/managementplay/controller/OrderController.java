package com.finalproject.managementplay.controller;

import com.alibaba.fastjson.JSON;
import com.finalproject.managementplay.bean.Company;
import com.finalproject.managementplay.bean.Order;
import com.finalproject.managementplay.bean.QueryInfo;
import com.finalproject.managementplay.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao cdao;

    @RequestMapping("/allorder")
    public String getOrderList(QueryInfo queryInfo){
        //获取最大列表数字和当前编号
        int numbers = cdao.getOrderCounts("%" + queryInfo.getQuery() + "%");
        int pageStart = (queryInfo.getPageNum() - 1) * queryInfo.getPageSize();

        List<Order> order = cdao.getAllOrder("%" + queryInfo.getQuery() + "%", pageStart, queryInfo.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", order);
        String res_string = JSON.toJSONString(res);
        return res_string;
    }

    @RequestMapping("/addorder")
    public String addCompany(@RequestBody Order order){

        int i = cdao.addOrder(order);
        return i > 0?"success":"error";
    }

    @RequestMapping("/getorderupdate")
    public String getUpdateOrder(int id){
        Order order = cdao.getUpdateOrder(id);
        String string = JSON.toJSONString(order);
        return string;
    }
    @RequestMapping("/editorder")
    public String editOrder(@RequestBody Order order){
        int i = cdao.editOrder(order);
        return i > 0?"success":"error";
    }


}
