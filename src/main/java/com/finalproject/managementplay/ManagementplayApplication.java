package com.finalproject.managementplay;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.finalproject.managementplay.dao")
@SpringBootApplication
public class ManagementplayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagementplayApplication.class, args);
    }

}
