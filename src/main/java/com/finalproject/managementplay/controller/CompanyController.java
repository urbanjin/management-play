package com.finalproject.managementplay.controller;

import com.alibaba.fastjson.JSON;
import com.finalproject.managementplay.bean.Company;
import com.finalproject.managementplay.bean.QueryInfo;
import com.finalproject.managementplay.bean.User;
import com.finalproject.managementplay.dao.CompanyDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class CompanyController {
    @Autowired
    private CompanyDao cdao;

    @RequestMapping("/allcompany")
    public String getCompanyList(QueryInfo queryInfo){
        //获取最大列表数字和当前编号
        int numbers = cdao.getCompanyCounts("%" + queryInfo.getQuery() + "%");
        int pageStart = (queryInfo.getPageNum() - 1) * queryInfo.getPageSize();

        List<Company> company = cdao.getAllCompany("%" + queryInfo.getQuery() + "%", pageStart, queryInfo.getPageSize());
        HashMap<String, Object> res = new HashMap<>();
        res.put("numbers", numbers);
        res.put("data", company);
        String res_string = JSON.toJSONString(res);
        return res_string;
    }

    @RequestMapping("/getcompanyupdate")
    public String getUpdateCompany(int id){
        Company company = cdao.getUpdateCompany(id);
        String string = JSON.toJSONString(company);
        return string;
    }

    @RequestMapping("/addcompany")
    public String addCompany(@RequestBody Company company){

        int i = cdao.addCompany(company);
        return i > 0?"success":"error";
    }
    @RequestMapping("/addcontact")
    public String addContact(@RequestBody Company company){

        int i = cdao.addContact(company);
        return i > 0?"success":"error";
    }

    @RequestMapping("/deletecompany")
    public String deleteCompany(int id){
        int i = cdao.deleteCompany(id);
        return i > 0?"success":"error";
    }

    @RequestMapping("/editcompany")
    public String editCompany(@RequestBody Company company){
        int i = cdao.editCompany(company);
        return i > 0?"success":"error";
    }

}
