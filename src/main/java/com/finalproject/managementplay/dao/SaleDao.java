package com.finalproject.managementplay.dao;

import com.finalproject.managementplay.bean.Sale;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaleDao {
    public List<Sale> getAllSale(@Param("name") String name, @Param("pageStart") int pageStart, @Param("pageSize") int pageSize);

    public int getSaleCounts(@Param("name") String name);
}
