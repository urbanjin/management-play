package com.finalproject.managementplay.dao;

import com.finalproject.managementplay.bean.Company;
import com.finalproject.managementplay.bean.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDao {

    public List<Order> getAllOrder(@Param("product") String product, @Param("pageStart") int pageStart, @Param("pageSize") int pageSize);

    public int getOrderCounts(@Param("product") String product);
    public int addOrder(Order order);
    public int editOrder(Order order);
    public Order getUpdateOrder(int id);
}
