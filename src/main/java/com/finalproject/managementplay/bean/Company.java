package com.finalproject.managementplay.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;

public class Company {

    private int id;
    private String companyName;
    private String sourceFrom;
    private String telephone;
    private String email;
    private String level;
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date nextContactDate;

    public Company() {
    }

    public Company(String companyName, String sourceFrom, String telephone, String email, String level, Date nextContactDate) {
        this.companyName = companyName;
        this.sourceFrom = sourceFrom;
        this.telephone = telephone;
        this.email = email;
        this.level = level;
        this.nextContactDate = nextContactDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Date getNextContactDate() {
        return nextContactDate;
    }

    public void setNextContactDate(Date nextContactDate) {
        this.nextContactDate = nextContactDate;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", sourceFrom='" + sourceFrom + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                ", level='" + level + '\'' +
                ", nextContactDate='" + nextContactDate + '\'' +
                '}';
    }
}
