package com.finalproject.managementplay.dao;

import com.finalproject.managementplay.bean.MainMenu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuDao {
    public List<MainMenu> getMenus();
    public List<MainMenu> getRegularMenus();
}
