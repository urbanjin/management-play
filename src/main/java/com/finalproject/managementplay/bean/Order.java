package com.finalproject.managementplay.bean;

import java.sql.Date;

public class Order {
    private int id;
    private String product;
    private int productNum;
    private String company;
    private Date date;

    public Order() {
    }

    public Order(String product, int productNum, String company, Date date) {
        this.product = product;
        this.productNum = productNum;
        this.company = company;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getProductNum() {
        return productNum;
    }

    public void setProductNum(int productNum) {
        this.productNum = productNum;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", product='" + product + '\'' +
                ", productNum=" + productNum +
                ", company='" + company + '\'' +
                ", date=" + date +
                '}';
    }
}
