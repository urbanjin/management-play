package com.finalproject.managementplay.dao;

import com.finalproject.managementplay.bean.Company;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyDao {

    public List<Company> getAllCompany(@Param("companyName") String companyName, @Param("pageStart") int pageStart, @Param("pageSize") int pageSize);
    public int getCompanyCounts(@Param("companyName") String companyName);
    public int addCompany(Company company);
    public int addContact(Company company);
    public int deleteCompany(int id);
    public Company getUpdateCompany(int id);
    public int editCompany(Company company);
}
